package info.rayhan.kingcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button digitBtn;
    TextView Display;
    String Operator;
    double afterOperator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display = (TextView) findViewById(R.id.CalculatorDisplay);

    }

    public void digitBtn(View view) {
        Button b = (Button) view;
        String buttonText = b.getText().toString();

        String displayText = Display.getText().toString();

        if (displayText.length() != 15){
            Display.setText(displayText + buttonText);
        }else{
            Toast.makeText(this,"15 character excedded!!",Toast.LENGTH_LONG).show();
        }


    }

    /**
     * clear text button C
     */

    public void clear(View view) {
        String displayText = Display.getText().toString();
        String newString = displayText.substring(0, displayText.length() - 1);
        Display.setText(newString);
    }

    /**
     * All clear Button
     */

    public void allClear(View views) {
        Display.setText("");
    }


    public void MathOperation(View rayhan) {
        Button btnTxt = (Button) rayhan;
        Operator = btnTxt.getText().toString();
        afterOperator = Double.parseDouble(Display.getText().toString());
        Display.setText("");
    }

    public void equalsBtn(View view) {

        double result = 0;
        double displayDigits = Double.parseDouble(Display.getText().toString());

        if (Operator.equals("+")) {
            result = displayDigits + afterOperator;
        } else if (Operator.equals("-")) {
            result = afterOperator - displayDigits;
        } else if (Operator.equals("*")) {
            result = displayDigits * afterOperator;
        } else if (Operator.equals("/")) {

            result = afterOperator / displayDigits;

        }else{
            result = Double.parseDouble(Display.getText().toString());
        }

        Display.setText("" + result);
    }

    public void dot(View view){
        String scVal = Display.getText().toString();
        boolean hasDot = false;
        char charArray [] = scVal.toCharArray();
        for(int i = 0 ; i < scVal.length() ; i++){
            if(charArray[i] == '.'){
                hasDot = true;
            }
        }

        if(hasDot != true){
            Display.setText(scVal+'.');
        }
    }

}
